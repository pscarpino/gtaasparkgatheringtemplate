package com.gta.spark.gathering.template

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.split
import scala.xml.XML
import org.apache.spark.sql.Row
import org.apache.spark.sql.types._
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.ForeachWriter

abstract class StructuredStreamingTemplate extends StreamingTemplate {
  
  def parseRow(line: String): String
  def correct(lines: Dataset[String]): DataFrame

  def process(spark: SparkSession, brokers: String, topic: String, storePath: String, checkpointLocationPath: String, interval: Int, consumerGroupId:String) {
    // Create DataSet representing the stream of input lines from kafka
    import spark.implicits._
    
    val lines = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", brokers)
      .option("subscribe", topic)
      .option("group.id", consumerGroupId)
      .load()
      .selectExpr("CAST(value AS STRING)")
      .as[String]
    
    val splitXML = udf( (body: String) => parseRow(body) )

    val parsed = lines.select( splitXML(lines.col("value")).as("value") ).as("value").as[String]
    
    val events = correct(parsed)
    
//    val query = events.writeStream.format("console").start()
    
    // Generate write Stream
    val query = events.writeStream
      .format("csv")
      .option("path", storePath + "_" + topic + "/")
      .option("checkpointLocation", checkpointLocationPath)
      .start()

    query.awaitTermination()
  }

}