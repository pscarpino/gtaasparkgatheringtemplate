package com.gta.spark.gathering.template

import org.apache.spark.sql.SparkSession

import com.gta.spark.gathering.template.instances.SparkStreamingYahooFinanceCSV
import com.gta.spark.gathering.template.instances.SparkStreamingYahooFinanceXML
import com.gta.spark.gathering.template.instances.StructuredStreamingYahooFinanceCSV
import com.gta.spark.gathering.template.instances.StructuredStreamingYahooFinanceXML

/*
 * spark-submit --master local[2]
 * --packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.2.0,org.apache.spark:spark-sql-kafka-0-10_2.11:2.2.0
 * --jars /Users/paoloscarpino/Documents/Programmazione/Programmi/SPARK_LIB/spark-streaming-kafka-assembly_2.11-1.6.3.jar
 * --class com.gta.spark.gathering.template.GTAASparkGatheringTemplate
 * /Users/paoloscarpino/eclipse-workspace/GTAASparkGatheringTemplate/target/GTAASparkGatheringTemplate-0.0.1-SNAPSHOT.jar
 * 127.0.0.1:9092 test1
 * 1 5
 * hdfs://127.0.0.1:9000/events hdfs://127.0.0.1:9000/checkpoint 
 * 0 test 
 *
 */

object GTAASparkGatheringTemplate {

  def getProcess(mode: Int, input_type: Int): StreamingTemplate = {
    if (mode == 1)
      if(input_type == 1)
        new StructuredStreamingYahooFinanceCSV()
      else
        new StructuredStreamingYahooFinanceXML()
    else
      if(input_type == 1)
        new SparkStreamingYahooFinanceCSV()
      else
        new SparkStreamingYahooFinanceXML()
  }

  /*
   * Main
   */
  def main(args: Array[String]) = {
    if (args.length < 7) {
      System.err.println(s"""
        |Usage: GTAASparkGatheringTemplate <brokers> <topic> <mode> <batch_interval> <hdfs_path> <checkpoint_hdfs_path> <input_mode> <consumer_group_id>
        |  <brokers> is a list of one or more Kafka brokers
        |  <topic> is one kafka topics to consume from
        |  <mode> is it possible to choose to use "Structured Streaming" (1) or "Spark Streaming" (2).
        |  <batch_interval> value expressed in seconds.
        |  <hdfs_path> the hdfs path used to store the events: e.g. hdfs://127.0.0.1:9000/events
        |  <checkpoint_hdfs_path> the hdfs path of checkpoint
        |  <input_mode> 1 CSV, 2 XML 
        |  <consumer_group_id> Consumer Group Id
        |
        """.stripMargin)
      System.exit(1)
    }

    // Retrieve args parameters
    val Array(brokers, topic, mode, interval, storePath, checkpointLocationPath, inputType, consumerGroupId) = args

    if (!interval.forall(_.isDigit)) {
      System.err.println(s"""
        |Usage: The batch_interval represents the seconds amongst the batchs and must be a decimal number
        |
        """.stripMargin)
      System.exit(1)
    }

    if (!mode.forall(_.isDigit)) {
      System.err.println(s"""
        |Usage: The mode must be a decimal value between a two fixed value (1, 2)
        |
        """.stripMargin)
      System.exit(1)
    } else if (!(Array(1, 2) contains mode.toInt)) {
      System.err.println(s"""
        |Usage: The mode must be a decimal value between a two fixed value (1, 2)
        |
        """.stripMargin)
      System.exit(1)
    }
    
    if (!inputType.forall(_.isDigit)) {
      System.err.println(s"""
        |Usage: The input_mode must be a decimal value between a two fixed value (1, 2)
        |
        """.stripMargin)
      System.exit(1)
    } else if (!(Array(1, 2) contains inputType.toInt)) {
      System.err.println(s"""
        |Usage: The input_mode must be a decimal value between a two fixed value (1, 2)
        |
        """.stripMargin)
      System.exit(1)
    }

    val streamingProcess = getProcess(mode.toInt, inputType.toInt)

    val spark = SparkSession
      .builder
      .appName("GTAASparkGatheringTemplate")
      .getOrCreate()

    streamingProcess.process(spark, brokers, topic, storePath, checkpointLocationPath, interval.toInt, consumerGroupId)
    
  }
}