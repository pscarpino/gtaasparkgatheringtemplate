package com.gta.spark.gathering.template.instances

import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col

import com.gta.spark.gathering.template.StructuredStreamingTemplate
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.split

class StructuredStreamingYahooFinanceCSV extends StructuredStreamingTemplate {

  def parseRow(line: String): String = {
    line
  }

  def correct(lines: Dataset[String]): DataFrame = {
    lines.withColumn("_tmp", split(col("value"), ",")).select(
      col("_tmp").getItem(0).as("date").cast(StringType),
      col("_tmp").getItem(1).as("open").cast(DoubleType),
      col("_tmp").getItem(2).as("high").cast(DoubleType),
      col("_tmp").getItem(3).as("low").cast(DoubleType),
      col("_tmp").getItem(4).as("close").cast(DoubleType),
      col("_tmp").getItem(5).as("volume").cast(IntegerType),
      col("_tmp").getItem(6).as("adjClose").cast(DoubleType)).drop("_tmp")
  }
}