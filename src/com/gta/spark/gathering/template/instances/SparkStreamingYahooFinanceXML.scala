package com.gta.spark.gathering.template.instances

import scala.xml.XML
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import com.gta.spark.gathering.template.SparkStreamingTemplate
import scala.collection.Seq
import org.apache.spark.sql.types.StringType

class SparkStreamingYahooFinanceXML extends SparkStreamingTemplate {

  def dfSchema(): StructType = {
    StructType(Seq(
      StructField(name = "date", dataType = StringType, nullable = false),
      StructField(name = "open", dataType = DoubleType, nullable = false),
      StructField(name = "high", dataType = DoubleType, nullable = false),
      StructField(name = "low", dataType = DoubleType, nullable = false),
      StructField(name = "close", dataType = DoubleType, nullable = false),
      StructField(name = "volume", dataType = IntegerType, nullable = false),
      StructField(name = "adj_close", dataType = DoubleType, nullable = false)
      )
    )
  }

  def parseRow(line: String): Row = {
    val xmlElems = XML.loadString(line)
    val date = (xmlElems \\ "root" \\ "date").text
    val open = (xmlElems \\ "root" \\ "open").text.toDouble
    val high = (xmlElems \\ "root" \\ "high").text.toDouble
    val low = (xmlElems \\ "root" \\ "low").text.toDouble
    val close = (xmlElems \\ "root" \\ "close").text.toDouble
    val volume = (xmlElems \\ "root" \\ "volume").text.toInt
    val adjClose = (xmlElems \\ "root" \\ "adj_close").text.toDouble
    Row(date, open, high, low, close, volume, adjClose)
  }

}