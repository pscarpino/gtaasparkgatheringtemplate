package com.gta.spark.gathering.template.instances

import scala.xml.XML

import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col

import com.gta.spark.gathering.template.StructuredStreamingTemplate
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.split

class StructuredStreamingYahooFinanceXML extends StructuredStreamingTemplate {

  
  def parseRow(line: String): String = {
    val xmlElems = XML.loadString(line)
    val date = (xmlElems \\ "root" \\ "date").text
    val open = (xmlElems \\ "root" \\ "open").text.toDouble
    val high = (xmlElems \\ "root" \\ "high").text.toDouble
    val low = (xmlElems \\ "root" \\ "low").text.toDouble
    val close = (xmlElems \\ "root" \\ "close").text.toDouble
    val volume = (xmlElems \\ "root" \\ "volume").text.toInt
    val adjClose = (xmlElems \\ "root" \\ "adj_close").text.toDouble
    date+","+open+","+high+","+low+","+close+","+volume+","+adjClose
  }
  
  def correct(lines: Dataset[String]): DataFrame = {
    lines.withColumn("_tmp", split(col("value"), ",")).select(
      col("_tmp").getItem(0).as("date").cast(StringType),
      col("_tmp").getItem(1).as("open").cast(DoubleType),
      col("_tmp").getItem(2).as("high").cast(DoubleType),
      col("_tmp").getItem(3).as("low").cast(DoubleType),
      col("_tmp").getItem(4).as("close").cast(DoubleType),
      col("_tmp").getItem(5).as("volume").cast(IntegerType),
      col("_tmp").getItem(6).as("adjClose").cast(DoubleType)).drop("_tmp")
  }
  
}