package com.gta.spark.gathering.template.instances

import scala.xml.XML
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import com.gta.spark.gathering.template.SparkStreamingTemplate
import scala.collection.Seq

class SparkStreamingYahooFinanceCSV extends SparkStreamingTemplate {

  def dfSchema(): StructType = {
    StructType(Seq(
      StructField(name = "date", dataType = StringType, nullable = false),
      StructField(name = "open", dataType = DoubleType, nullable = false),
      StructField(name = "high", dataType = DoubleType, nullable = false),
      StructField(name = "low", dataType = DoubleType, nullable = false),
      StructField(name = "close", dataType = DoubleType, nullable = false),
      StructField(name = "volume", dataType = IntegerType, nullable = false),
      StructField(name = "adj_close", dataType = DoubleType, nullable = false)
      )
    )
  }

  def parseRow(line: String): Row = {
    val csvElems = line.split(",")
    val date = csvElems(0).toString()
    val open = csvElems(1).toDouble
    val high = csvElems(2).toDouble
    val low = csvElems(3).toDouble
    val close = csvElems(4).toDouble
    val volume = csvElems(5).toInt
    val adjClose = csvElems(6).toDouble
    Row(date, open, high, low, close, volume, adjClose)
  }

}