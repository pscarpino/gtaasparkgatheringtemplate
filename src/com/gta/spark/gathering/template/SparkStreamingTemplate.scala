package com.gta.spark.gathering.template

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.kafka010.KafkaUtils
import scala.xml.XML
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.kafka.common.TopicPartition

abstract class SparkStreamingTemplate extends StreamingTemplate {

  def dfSchema(): StructType
  def parseRow(line: String): Row

  def process(spark: SparkSession, brokers: String, topic: String, storePath: String, checkpointLocationPath: String, interval: Int, consumerGroupId: String) {
    import spark.implicits._

    val ssc = new StreamingContext(spark.sparkContext, Seconds(interval))
    ssc.checkpoint(checkpointLocationPath)

    // Create direct kafka stream with brokers and topics
    val topicsSet = topic.split(",").toSet
    //val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers, "group.id" -> consumerGroupId)

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> brokers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> consumerGroupId,
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean))

    //val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicsSet)

    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topic.split(","), kafkaParams));

    messages.map(record => (record.value))
            .foreachRDD { rdd =>
      if (rdd.count() > 0) {
        val tmp = rdd.map(l => parseRow(l.toString()))
        val df = spark.createDataFrame(tmp, dfSchema)
        df.write
          .format("csv")
          .mode(org.apache.spark.sql.SaveMode.Append)
          .save(storePath + "_" + topic + "/")
      }
    }

    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }

}