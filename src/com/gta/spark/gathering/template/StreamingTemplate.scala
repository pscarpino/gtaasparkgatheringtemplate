package com.gta.spark.gathering.template

import org.apache.spark.sql.SparkSession

trait StreamingTemplate extends Serializable {
  
  def process(spark:SparkSession, brokers:String, topics:String, storePath:String, checkpointLocationPath:String, interval:Int, consumerGroupId:String)
}